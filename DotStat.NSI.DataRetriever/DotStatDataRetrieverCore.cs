﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using DotStat.Common.Logger;
using DotStat.Db;
using DotStat.Db.Exception;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DryIoc;
using Estat.Nsi.DataRetriever;
using Estat.Sdmxsource.Extension.Engine;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace DotStat.NSI.DataRetriever
{
    public class DotStatDataRetrieverCore : ISdmxDataRetrievalWithWriter
    {
        private readonly Container _container;
        private readonly IHeaderRetrieverEngine _headerRetrieverEngine;
        private readonly IHeader _defaultHeader;
        private readonly Tuple<long?, long?> _rangeScope;
        private readonly string _acceptHeaderOfRequest;
        private readonly string _dataSpace;
        private readonly IObservationRepository _observationRepository;
        private readonly ISdmxDataRetrievalWithWriter _sriDataRetriever;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly ConnectionStringSettings _conn;
        private readonly IPrincipal _principal;
        private const string NaN = "NaN";
        private readonly TargetVersion _targetVersion;

        public DotStatDataRetrieverCore(
            IPrincipal principal,
            Container container,
            IHeader defaultHeader,
            IHeaderRetrieverEngine headerRetrieverEngine,
            ISdmxDataRetrievalWithWriter sriDataRetriever,
            ConnectionStringSettings conn,
            Tuple<long?, long?> rangeScope,
            string acceptHeaderOfRequest,
            TargetVersion? targetVersion
        )
        {
            _container = container ?? throw new ArgumentNullException(nameof(container));
            _defaultHeader = defaultHeader ?? throw new ArgumentNullException(nameof(defaultHeader));
            _headerRetrieverEngine = headerRetrieverEngine ?? throw new ArgumentNullException(nameof(headerRetrieverEngine));
            _principal = principal;
            _conn = conn;
            _rangeScope = rangeScope;
            _acceptHeaderOfRequest = acceptHeaderOfRequest;
            _targetVersion = targetVersion ?? TargetVersion.Live;
            _sriDataRetriever = sriDataRetriever;

            _dataSpace = ConfigurationManager.AppSettings["DataspaceId"];
            _observationRepository = _container.Resolve<IObservationRepository>();
            _mappingStoreDataAccess = _container.Resolve<IMappingStoreDataAccess>();

            Log.SetDataspaceId(_dataSpace);
        }

        public void GetData(IDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            //Check if dataflow has attached MappingSet, if true then query data using the default data retriever, if not the us the DotstatV8 data retriever
            if (_mappingStoreDataAccess.HasMappingSet(dataQuery.Dataflow))
                _sriDataRetriever.GetData(dataQuery, dataWriter);
            else
                GetDotStatV8GetData(dataQuery, dataWriter);
        }

        private string WriteObservation(IDataWriterEngine dataWriter, IObservation observation, string previousKey, string obsDimension,
            IList<IKeyable> dimensionAttributeKeyables, bool writeNaNForNullObservationValues)
        {
            var seriesKey = observation.SeriesKey;

            var currentKey = seriesKey.GetKey();

            if (!currentKey.Equals(previousKey))
            {
                // Start series
                dataWriter.StartSeries();

                // Series key
                foreach (var dim in seriesKey.Key)
                {
                    dataWriter.WriteSeriesKeyValue(dim.Concept, dim.Code);
                }

                // Series level attributes
                if (seriesKey.Attributes != null)
                {
                    foreach (var seriesAttribute in seriesKey.Attributes)
                    {
                        if (!string.IsNullOrEmpty(seriesAttribute.Code))
                        {
                            dataWriter.WriteAttributeValue(seriesAttribute.Concept, seriesAttribute.Code);
                        }
                    }
                }

                if (dimensionAttributeKeyables.Any())
                {
                    // When there are any attributes saved to be reported at series level then write the ones that has matching keys with the current series key
                    foreach (var attributeKeyable in dimensionAttributeKeyables)
                    {
                        var keyMatch = attributeKeyable.Key.All(dimensionKeyValue =>
                            seriesKey.GetKeyValue(dimensionKeyValue.Concept).Equals(dimensionKeyValue.Code));

                        if (!keyMatch)
                        {
                            continue;
                        }

                        // Dimension and (depending on the output format) group level attributes
                        foreach (var dimensionAttribute in attributeKeyable.Attributes)
                        {
                            if (!string.IsNullOrEmpty(dimensionAttribute.Code))
                            {
                                dataWriter.WriteAttributeValue(dimensionAttribute.Concept, dimensionAttribute.Code);
                            }
                        }
                    }
                }
            }

            dataWriter.WriteObservation(obsDimension, observation.ObsTime,
                string.IsNullOrEmpty(observation.ObservationValue) && writeNaNForNullObservationValues
                    ? NaN
                    : observation.ObservationValue);

            // Observation level attributes
            if (observation.Attributes != null)
            {

                foreach (var attrValue in observation.Attributes)
                {
                    if (!string.IsNullOrEmpty(attrValue.Code))
                    {
                        dataWriter.WriteAttributeValue(attrValue.Concept, attrValue.Code);
                    }
                }
            }

            return currentKey;
        }

        private IHeader GetHeader(IDataQuery dataQuery)
        {
            try
            {
                return _headerRetrieverEngine.GetHeader(dataQuery, null, null);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DataRetrieverException(
                    ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError),
                    "Error populating header");
            }
        }

        public int GetNumberOfObservations(IDataQuery dataQuery)
        {
            try
            {
                return _observationRepository.GetObservationsCount(dataQuery, _dataSpace, _targetVersion);
            }
            catch (ArtefactNotFoundException ex)
            {
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound), "No data found");
            }

        }

        private void ValidateAuthorization(IDataflowObject dataflow)
        {
            //var isAuthorized = _authorizationManagement.IsAuthorized(
            //    _principal, 
            //    _dataSpace, 
            //    dataflow.AgencyId, 
            //    dataflow.Id, 
            //    dataflow.Version, 
            //    PermissionType.CanReadData
            //);

            //if (!isAuthorized)
            //    throw new DataRetrieverException("Unauthorised", SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.Unauthorised));
        }


        private void GetDotStatV8GetData(IDataQuery dataQuery,
            IDataWriterEngine dataWriterEngine)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException(nameof(dataQuery));
            }

            if (dataWriterEngine == null)
            {
                throw new ArgumentNullException(nameof(dataWriterEngine));
            }

            try
            {
                ValidateAuthorization(dataQuery.Dataflow);

                var rangeStart = _rangeScope?.Item1;
                if (rangeStart.HasValue)
                {
                    if (rangeStart != 0)
                    {
                        throw new SdmxNotImplementedException("Range start not equal to 0.");
                    }

                    rangeStart++; // Range header contains 0-bases indexes
                }

                var rangeEnd = _rangeScope?.Item2;
                if (rangeEnd.HasValue)
                {
                    rangeEnd++; // Range header contains 0-bases indexes
                }
                
                using (var observationEnumerator = _observationRepository
                    .GetObservations(dataQuery, _dataSpace, _targetVersion, rangeStart, rangeEnd).GetEnumerator())
                {
                    if (!observationEnumerator.MoveNext())
                    {
                        throw new SdmxNoResultsException("NoRecordsFound");
                    }

                    var messageHeader = GetHeader(dataQuery) ?? _defaultHeader;

                    Log.Debug($"Range: {_rangeScope?.Item1}/{_rangeScope?.Item2}");

                    var isJsonOrCsv = !string.IsNullOrWhiteSpace(_acceptHeaderOfRequest) &&
                                  (_acceptHeaderOfRequest.Contains("json") || _acceptHeaderOfRequest.Contains("csv"));

                    var isAllDimensionsAtObservationRequested = dataQuery.DimensionAtObservation.Equals(DatasetStructureReference.AllDimensions,
                        StringComparison.OrdinalIgnoreCase);

                    var specialTreatmentForGroupAttributes = isJsonOrCsv || isAllDimensionsAtObservationRequested;

                    dataWriterEngine.WriteHeader(messageHeader);

                    var dsr = new DatasetStructureReferenceCore(string.Empty,
                        dataQuery.DataStructure.AsReference, null, null, dataQuery.DimensionAtObservation);

                    var header = new DatasetHeaderCore(messageHeader.DatasetId, DatasetActionEnumType.Information,
                        dsr);

                    dataWriterEngine.StartDataset(dataQuery.Dataflow, dataQuery.DataStructure, header);

                    var attributeRepository = _container.Resolve<IAttributeRepository>();

                    // Dataset level attributes
                    foreach (var datasetAttributeKeyValue in attributeRepository.GetDatasetAttributes(dataQuery, _dataSpace, _targetVersion))
                    {
                        dataWriterEngine.WriteAttributeValue(datasetAttributeKeyValue.Concept, datasetAttributeKeyValue.Code);
                    }

                    var dimensionAttributeKeyables = new List<IKeyable>();

                    foreach (var attributeKeyable in attributeRepository.GetKeyables(dataQuery, _dataSpace, _targetVersion))
                    {
                        if (attributeKeyable.Series)
                        {
                            // Dimension level attributes are collected to be able to present them at series in output file
                            dimensionAttributeKeyables.Add(attributeKeyable);
                            continue;
                        }

                        if (specialTreatmentForGroupAttributes)
                        {
                            // Workaround for JSON writer where existence of group name prevents writing keyable
                            // If needed group level attributes are collected to be able to present them at series in output file
                            var attributeKeyableWithoutGroupName = new KeyableImpl(attributeKeyable.Dataflow,
                                attributeKeyable.DataStructure, attributeKeyable.Key,
                                attributeKeyable.Attributes, attributeKeyable.TimeFormat, attributeKeyable.Annotations.ToArray());

                            dimensionAttributeKeyables.Add(attributeKeyableWithoutGroupName);
                            continue;
                        }

                        // When no special treatment is need for group level attributes they are immediately written (e.g. SDMX-ML series format)
                        dataWriterEngine.StartGroup(attributeKeyable.GroupName);

                        foreach (var dimKeyValue in attributeKeyable.Key)
                        {
                            dataWriterEngine.WriteGroupKeyValue(dimKeyValue.Concept, dimKeyValue.Code);
                        }

                        // Group level attributes
                        foreach (var attributeKeyValue in attributeKeyable.Attributes)
                        {
                            dataWriterEngine.WriteAttributeValue(attributeKeyValue.Concept, attributeKeyValue.Code);
                        }
                    }

                    var obsDimension = isAllDimensionsAtObservationRequested
                        ? dataQuery.DataStructure.TimeDimension?.Id
                        : dataQuery.DimensionAtObservation;

                    var previousKey = "";

                    bool.TryParse(ConfigurationManager.AppSettings["WriteNaNForNullObservationValues"], out bool writeNaNForNullObservationValues);

                    // Use only for sdmx-ml
                    writeNaNForNullObservationValues = !isJsonOrCsv && writeNaNForNullObservationValues;

                    // Write observation - enumerator points already to the first observation
                    do
                    {
                        previousKey = WriteObservation(dataWriterEngine, observationEnumerator.Current, previousKey, obsDimension,
                            dimensionAttributeKeyables, writeNaNForNullObservationValues);
                    } while (observationEnumerator.MoveNext());
                }
            }
            catch (ArtefactNotFoundException)
            {
                throw new SdmxNoResultsException("No data found");
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                dataWriterEngine.Close(
                    new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                        Severity.Error,
                        new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (SdmxResponseTooLargeException e)
            {
                dataWriterEngine.Close(
                    new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                        Severity.Error,
                        new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                throw new DataRetrieverException(ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError),
                    "Error executing generated SQL and populating SDMX model");
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DataRetrieverException(ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError),
                    "Error during writing response");
            }
            finally
            {
                dataWriterEngine.Close();
            }
        }

                
    }
}