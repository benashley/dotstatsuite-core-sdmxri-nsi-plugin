﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Db.Manager.SqlServer;
using DotStat.Db.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.MappingStore;
using DryIoc;
using log4net;
using Microsoft.Extensions.Configuration;

namespace DotStat.NSI.DataRetriever.Helper
{
    public class DotStatDataRetrieverIoc
    {
        public Container CreateContainer()
        {
            var container = new Container();

            SetupConfiguration(container);

            container.Register<IDbManager, SqlServerDbManager>(Reuse.Singleton);
            container.Register<IMappingStoreDataAccess, MappingStoreDataAccess>(Reuse.Singleton);
            container.Register<IObservationRepository, SqlObservationRepository>(Reuse.Singleton);
            container.Register<IAttributeRepository, SqlAttributeRepository>(Reuse.Singleton);

            return container;
        }

        // Todo consider migrating this functions to DotStat.Common / DotStat.Access
        private void SetupConfiguration(IContainer container)
        {
            var configuration = BuildConfiguration(ConfigurationManager.AppSettings["ConfigDirectory"]).Get<BaseConfiguration>();

            // ----------------------------------------------------------

            container.UseInstance(configuration);
            container.UseInstance<IDataspaceConfiguration>(configuration);
            container.UseInstance<ILocalizationConfiguration>(configuration);
            container.UseInstance<IMailConfiguration>(configuration);
            container.UseInstance<IGeneralConfiguration>(configuration);

            LocalizationRepository.Configure(configuration);

            //Log4Net 
            const string applicationName = "sdmxri-nsi-ws";
            GlobalContext.Properties[CustomParameter.Application.ToString()] = applicationName;

            LogHelper.ConfigureAppenders(configuration);

            Log.Info($"{applicationName} application configured. Version number: {Assembly.GetExecutingAssembly().GetName().Version}");

            Log.Info($"Dataspaces defined in configuration: [{string.Join(',',configuration.SpacesInternal.Select(s => s.Id))}]"); 
        }

        private IConfigurationRoot BuildConfiguration(string path)
        {
            Log.Info($"Building configuration from '{path}'");

            if (string.IsNullOrEmpty(path))
                throw new ArgumentException(nameof(path));

            if(!Directory.Exists(path))
                throw new InvalidOperationException($"Config directory '{path}' doesn't exist");

            var builder = new ConfigurationBuilder();

            foreach (var json in Directory.GetFiles(path, "*.json"))
                builder.AddJsonFile(json);

            builder.AddEnvironmentVariables();

            return builder.Build();
        }
    }
}