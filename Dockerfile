#---- sdmxri-nsi plugin build ---------------------------------------------------------
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app

ARG NUGET_FEED=https://api.nuget.org/v3/index.json

COPY DotStat.NSI.RetrieverFactory/*.csproj ./DotStat.NSI.RetrieverFactory/
COPY DotStat.NSI.DataRetriever/*.csproj ./DotStat.NSI.DataRetriever/

RUN dotnet restore --source $NUGET_FEED DotStat.NSI.RetrieverFactory

# copy everything else
COPY . ./
WORKDIR /app/DotStat.NSI.RetrieverFactory
RUN dotnet publish -c Release -o out --no-restore

#---- runtime ---------------------------------------------------------
FROM siscc/sdmxri-nsi-maapi:7.10.3 AS runtime
WORKDIR /app

#-- copy plugin binaries
COPY --from=build /app/DotStat.NSI.RetrieverFactory/out/DotStat.* \
/app/Plugins/

ENTRYPOINT ["./entrypoint.sh"]
