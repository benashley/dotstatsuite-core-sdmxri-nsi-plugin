# Nsi plugin
## NSI plugin Installation
[Installation document](docs/installation/CodeBaseApproach.md)

## NSI plugin Configuration
The NSI dotstat plugin configuration should be added to the file `/config/dataspaces.private.json` as a json node. 

### Keys
Name|Description|Type|Required
---|---|---|---
SpacesInternal|List of spaces ex. capture, stage, design, disseminate. See further information in table [SpacesInternal](# SpacesInternal)|DataspaceInternal|Yes

#### SpacesInternal

Name|Description|Type|Required
---|---|---|---
Id|Unique identifier for the data space, ex. "design"|string|Yes
DotStatSuiteCoreStructDbConnectionString|Connection string for the mapping store database (structure database)|string|Yes
DotStatSuiteCoreDataDbConnectionString|Connection string for the data database|string|Yes
DataImportTimeOutInMinutes|Max. time in minutes for a data import transaction|int|Yes
DatabaseCommandTimeoutInSec|Max. time in seconds for a single SQL command execution|int|Yes
AutoLog2DB|Specify if the log4net logging configuration should be generated automatically to log into the Data DB|bool|No (Default false)
AllowPITTargetDataVersion|Specify if access to Point-in-Time data should be allowed. If false, attempts to do so result in a 401|bool|No (Default true)

### Example

The following is a basic configuration example for an NSI service, configured to serve one dataspace with id *design*
```json
{
  "spacesInternal": [
    {
      "Id": "design",
      "DotStatSuiteCoreStructDbConnectionString": "Data Source=localhost;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX",
      "DotStatSuiteCoreDataDbConnectionString": "Data Source=localhost;Initial Catalog=DATA_DB;User ID=USERNAME;Password=XXXXXX",
      "DataImportTimeOutInMinutes": 60,
      "DatabaseCommandTimeoutInSec": 60
    }
  ]
}
```

## Register configuration
Inside the node "appSettings", in the NSI webservice app.config, add the following two lines.
```xml
<appSettings>
    ...
    <add key="DataspaceId" value="dataspaceid"/>
    <add key="ConfigDirectory" value="C:\Apps\NSIWebServicePath\config\" /> 
</appSettings>
```


## Register dotstat retriever
Inside the node "retrievers" inside "module", in the NSI webservice app.config, add the following line.
```xml
<module>
    <retrievers>
        ...
	    <add name="DotStatRetrieverFactory"/>	      
    </retrievers>
</module>
```

## Register MappingStoreServer
Inside the node "connectionStrings" inside "configuration", in the NSI webservice app.config, add the following line.
```xml
<configuration>
    <connectionStrings>
        ...
        <add name="MappingStoreServer" connectionString="Data Source=STRUCTURE_DB_HOST;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX;" providerName="System.Data.SqlClient"/>
    </connectionStrings>
</configuration>
```

### Logging configurarion
How to modify the default logging configuration? [See more.](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/blob/develop/docs/readme.md)