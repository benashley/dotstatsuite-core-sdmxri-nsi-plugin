# Installing DotstatSuite-Core-Sdmxri-Nsi-Plugin service with the Codebase approach

## Purpose of this document
The goal of this document to assist you setting up DotstatSuite-Core-sdmx-nsiplugin using the codebase approach.

## Content of this document
- Prerequisites
- Database
- Service
- Configuration

## Prerequisites
- IIS server 7.5 or later
- dotnet-hosting-2.2.4-win installed on web server
- Related NSI webservice (Build or download it)

## Database
### Structure database 
Install/update the structure database related to the NSI webservice. 

### Data database
Follow the instructions install a **DotStatSuiteCore Data Database** [See steps](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-db/blob/release-1.0.0/docs/installation/CodeBaseApproach.md)
.

## Service
The current DotstatSuite-Core-sdmx-nsiplugin (release-1.0.0) is based on the  NSIWebservice 7.4.

### Build the plugin
Compile and publish the solution with latest visual studio 2017, or run the "dotnet publish" command in the root folder of the DotstatSuite-Core-sdmx-nsiplugin repository.

Take the following binaries from the publish folder. By default it should be : "c:\git\dotstatsuite-core-sdmxri-nsi-plugin\DotStat.NSI.RetrieverFactory\bin\Debug\netcoreapp2.2\publish\"
- DotStat.Common.dll
- DotStat.DB.dll
- DotStat.Domain.dll
- DotStat.MappingStore.dll
- DotStat.NSI.DataRetriever.dll
- DotStat.NSI.RetrieverFactory.dll
- MicroKnights.Log4NetAdoNetAppender.dll
- DotStat.NSI.RetrieverFactory.deps.json

Copy these files to the plugin folder of the NSI Webservice.


### Update existing webservice
If you already have a previous version of NSIWebservice in an existing .Stat Suite deployment, you can update the existing NSIWebservice:
- Please create a backup of the folder of your existing NSIWebservice.
- Remove all files from the folder of your existing NSIWebservice.
- Copy the new version of NSIWebservice to the folder.
- Copy the built files to the NSIWebservice's plugin folder.

### Install new webservice
If you are installing a new instance of NSIWebservice, please follow these steps:
- Create the folder for NSIWebservice on the Web hosting machine. 
- Copy the built binaries to the NSIWebservice's plugin folder.
- Open Internet Information Service on the the Web hosting machine.
- Create Application pool for NSIWebservice. 
- Set application pool properties. the value for ".Net Clr version" should be "No managed code"
- Create application for NSIWebservice. Use the application pool created in previous step for application pool. Use the folder of the NSIWebservice for Physical path.
- Open file explorer, and edit the properties of the folder containing the NSIWebservice. 
- Grant full control access for the local machine's IUSR and IIS_IUSRS users.

## Configuration
The configuration files of the NSIWebservcie are in the config folder inside the NSIWebservice folder.
### App.config
Open the app.config file and do the following changes:
- Remove 
```
    <sectionGroup name="estat.nsi.ws.config">
      <section name="auth" type="Estat.Nsi.AuthModule.AuthConfigSection, Estat.Nsi.AuthModule"/>
    </sectionGroup>
```
- Replace: 
``` 
    <!-- Replace HOST with hostname or IP address of the server/PC running mapping store database -->
    <!-- Replace MADB with Mapping store database -->
    <!-- Replace USER with Mapping store database user name -->
    <!-- Replace PASS with Mapping store database user password -->
    <!-- Uncomment only one <add.../> at a time.-->
    <!-- Sql Server without any instance-->
    <!--<add name="MappingStoreServer" connectionString="Data Source=HOST;Initial Catalog=MADB;Integrated Security=False;User ID=USER;Password=PASS"
     providerName="System.Data.SqlClient" />-->
    <!-- Sql Server Express-->
    <!--<add name="MappingStoreServer" connectionString="Data Source=HOST\sqlexpress;Initial Catalog=MADB;Integrated Security=False;User ID=USER;Password=PASS"
     providerName="System.Data.SqlClient" />-->
    <!-- Sql Server Express on localhost using integral security-->
    <!--<add name="MappingStoreServer" connectionString="Data Source=HOST\sqlexpress;Initial Catalog=MADB;Integrated Security=False;User ID=USER;Password=PASS"
     providerName="System.Data.SqlClient" />-->
    <!-- Oracle Express using the Oracle ODP.NET provider -->
    <!--<add name="MappingStoreServer" connectionString="Data Source=HOST/MADB;User ID=USER;Password=PASS"
         providerName="Oracle.ManagedDataAccess.Client" />-->
    <!-- MySQL -->
    <!--<add name="MappingStoreServer" connectionString="server=HOST;user id=USER;password=PASS;database=MADB" providerName="MySql.Data.MySqlClient"/>-->
    <!-- The same applies to Authorization/Authentication database. Default name : 'authdb' but can be changed at dbAuth config below-->
    <!-- Example of using authtest db on localhost sqlexpress db-->
    <!--<add name="authdb" connectionString="Data Source=.\SQLEXPRESS;Initial Catalog=authtest;Integrated Security=False;User ID=USER;Password=PASS" providerName="System.Data.SqlClient" />-->
```
To 
```
<add name="MappingStoreServer" connectionString="Data Source={yourDBServer};Initial Catalog={YourStructureDatabase};User Id={YourStrucuterDatabaseWriteruser};Password={YourStrucuterDatabaseWriterpassword};" providerName="System.Data.SqlClient"/>     
```
- Replace
```
<value>SOME_NSI</value>
```
To
```
<value>.Stat V8</value>
```
- Replace
```
<add key="enableSubmitStructure" value="false"/>
```
To
```
<add key="enableSubmitStructure" value="true"/>
```
- Replace
```
<add key="configLocation" value="c:\ProgramData\Eurostat\nsiws.config"/> 
```
To
```
<!--<add key="configLocation" value="c:\ProgramData\Eurostat\nsiws.config"/>--> 
```
- Replace
```
<add key="corsSettings" value="false"/>
```
To
```
<add key="corsSettings" value="true"/>
```
- Replace
```
	<add key="estat.sri.ws.policymodule" value="true"/>
    <!--A comma separated string that determines the order for the middleware implementations -->
    <add key="middlewareImplementation" value="CorsMiddlewareBuilder,AuthMiddlewareBuilder,PolicyModuleMiddlewareBuilder"/>
```	
To
```

    <add key="estat.sri.ws.policymodule" value="false"/>
    <add key="middlewareImplementation" value="CorsMiddlewareBuilder"/>
    <!-- OECD Data plugin config -->
    <add key="DataspaceId" value="design" />
    <add key="ConfigDirectory" value="config" />
```
- Remove
```
<estat.nsi.ws.config>
    <!-- authentication configuration-->
    <auth anonymousUser="" realm="nsiws">
      <userCredentialsImplementation type="UserCredentialsHttpBasic"/>
      <!-- The AUTH DB provider. It looks for a connection string with name equal to "authdb"-->
      <authenticationImplementation type="EstatSriSecurityAuthenticationProvider"/>
      <!-- Legacy options that use the Auth Schema and Test Auth config -->
      <!--
      <authenticationImplementation type="DbAuthenticationProvider"/>
      <authorizationImplementation type="DbAuthorizationProvider"/>
      <dbAuth>
        <authentication sql="select password from users where userid=${user}" connectionStringName="authdb"/>
        <authorization sql="select d.id as ${id}, d.ver as ${version}, d.agency as ${agencyId} from users u inner join user_df ud on u.pk=ud.user_pk inner join dfs d on d.pk = ud.df_pk where u.userid=${user}"/>
      </dbAuth>
      -->
    </auth>
  </estat.nsi.ws.config>
```
- Replace
```
    <corsCollection>
      <!-- Allow CORS to any caller, to restrict change * to allowed domain, if multiple allowed domains can access NSI webservice then use as many add nodes as needed -->
      <!-- <add domain="http://localhost:3030" allowed-methods="GET,POST" allowed-headers="Range" /> -->
      <!-- <add domain="*" allowed-methods="GET,POST,PUT" allowed-headers="Range, Origin, Authorization" allow-credentials="true" exposed-headers="Location" /> -->
    </corsCollection>
```
To
```
    <corsCollection>
        <add domain="*" allowed-methods="GET,POST,PUT" allowed-headers="Range, Origin, Authorization" allow-credentials="true" exposed-headers="Location" />
    </corsCollection>
```
- Replace
```
    <retrievers>
      <!-- First provider to be tried -->
      <add name="MappingStoreRetrieversFactory"/>
      <!-- Second provider to be tried. It will tried only if first cannot serve the result.-->
      <!-- <add name="SomeFactoryId"/>-->
      <!-- Third -->
      <!-- etc. -->
    </retrievers>
```
To
```
    <retrievers>
        <add name="DotStatRetrieverFactory"/>
    </retrievers>
```
Save the file
### dataspaces.private.json
Create a new file in the config fodler called dataspaces.private.json
Edit the content. It should be like:
```
{
    "MaxTransferErrorAmount": 100,
    "DefaultLanguageCode": "en",
    "SmtpHost": "",
    "SmtpPort": 25,
    "SmtpEnableSsl": false,
    "SmtpUserName": "",
    "SmtpUserPassword": "",
    "MailFrom": "",    
    "spacesInternal": [
    {
      "Id": "design",      
	  "DotStatSuiteCoreStructDbConnectionString":"Data Source={YourDataDatabaseServer};Initial Catalog={YourdesignStructureDatabase};User Id={YourdesignStrucuterDatabaseWriteruser};Password={YourdesignStrucuterDatabaseWriterpassword};",
	  "DotStatSuiteCoreDataDbConnectionString": "Data Source={YourDatabaseServer};Initial Catalog={YourdesignDataDatabase};User Id={YourdesignDataDatabaseWriteruser};Password={YourdesignDataDatabaseWriterpassword};", 
      "DataImportTimeOutInMinutes": 60,      
      "DatabaseCommandTimeoutInSec": 60
    },
    {
      "Id": "disseminate",      
	  "DotStatSuiteCoreStructDbConnectionString":"Data Source={YourDatabaseServer};Initial Catalog={YourdisseminateStructureDatabase};User Id={YourdisseminateStrucuterDatabaseWriteruser};Password={YourdisseminateStrucuterDatabaseWriterpassword};",
	  "DotStatSuiteCoreDataDbConnectionString": "Data Source={YourDatabaseServer};Initial Catalog={YourdisseminateDataDatabase};User Id={YourdisseminateDataDatabaseWriteruser};Password={YourdisseminateDataDatabaseWriterpassword};", 
      "DataImportTimeOutInMinutes": 60,
      "DatabaseCommandTimeoutInSec": 60
    }
  ]
}
```
Save the file
### localization.json
Create a new file in the config fodler called localization.json
Edit the content. It should be like:
```
{
  "localizedResources": [
    {
      "Id": "MailSubject",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Data Lifecycle Manager transfer request ID - '{0}', target space '{1}'"
    },
    {
      "Id": "MailBody",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "<p>Dear DLM user, your transfer has been processed in the system, please see below the details:</p><table style=\"padding:5px\"><tr><th>Date</th><th>Level</th><th>Message</th></tr>{0}</table>"
    },
    {
      "Id": "SubmissionResult",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Successfully registered request with ID: {0}"
    },
    {
      "Id": "DataflowLoaded",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Successfully loaded structure information of dataflow '{0}' from dataspace '{1}'."
    },
    {
      "Id": "StreamingFinished",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Streaming succeeded."
    },
    {
      "Id": "PluginProcessFinished",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Successfully verified/transformed observations."
    },
    {
      "Id": "TransactionAbortedDueToConcurrentTransaction",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Aborting current transaction with ID {0}. There is already an active transaction in progress for DSD ID {1} with transaction ID {2}."
    },
    {
      "Id": "PreviousTransactionTimedOut",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "The previous transaction (ID {0}) targeting the same DSD ID {1} timed out so has been terminated."
    },
    {
      "Id": "TransactionSuccessful",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Transaction closed successfully."
    },
    {
      "Id": "NoActiveTransactionWithId",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "There is no active transaction to close with ID {0}."
    },
    {
      "Id": "TerminatingTimedOutTransaction",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Terminating timed out transaction ID {0})."
    },
    {
      "Id": "CodeNotFoundForDimension",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Code [{0}] not found for dimension {1}."
    },
    {
      "Id": "DimensionNotFoundByCodeListTranslator",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Dimension code [{0}] not found by code list translator."
    },
    {
      "Id": "DataflowNotFoundInDataspace",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Dataflow [{0}:{1}({2})] not found in [{3}] dataspace."
    },
    {
      "Id": "DimensionDoesNotBelongToDataset",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "The dimension {0} does not belong to dataset {1}."
    },
    {
      "Id": "AttributeDoesNotBelongToDataset",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "The attribute {0} does not belong to dataset {1}."
    },
    {
      "Id": "MissingCodeMember",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Missing member for [{3}] dimension, at row [{0}]"
    },
    {
      "Id": "UnknownCodeMember",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Unknown member for [{3}], at row [{0}]"
    },
    {
      "Id": "ConstraintViolation",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Constraint violation for [{3}], at row [{0}]"
    },
    {
      "Id": "NullValue",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Mandatory attribute / observation value empty, Coordinate: [{1}], at row [{0}]"
    },
    {
      "Id": "InvalidValueFormat",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Observation value format error [{2}], at row [{0}]"
    },
    {
      "Id": "DuplicatedCoordinate",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Dublicate coordinate [{1}], at row [{0}]"
    },
    {
      "Id": "TransactionAbortedDueToConcurrentArtefactAccess",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Aborting transaction due to concurrent process creating the same artefact: {0}"
    },
    {
      "Id": "ChangeInDsdCommonMessage",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Change has been detected on DSD {0} since the last dataload: "
    },
    {
      "Id": "ChangeInDsdDimensionToTimeDimension",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Dimension changed to time dimension: {0}"
    },
    {
      "Id": "ChangeInDsdDimensionAdded",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "New dimension added: {0}"
    },
    {
      "Id": "ChangeInDsdDimensionRemoved",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Dimension(s) removed: {0}"
    },
    {
      "Id": "ChangeInDsdDimensionCodelistChanged",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Code list of dimension {0} changed to {1}."
    },
    {
      "Id": "ChangeInDsdTimeDimensionAdded",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Time dimension has been added to the DSD."
    },
    {
      "Id": "ChangeInDsdTimeDimensionRemoved",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Time dimension has been removed from the DSD."
    },
    {
      "Id": "ChangeInDsdAttributeAdded",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "New attribute added: {0}"
    },
    {
      "Id": "ChangeInDsdAttributeCodedRepresentationChanged",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Representation of attribute {0} changed from {1} coded to {2} coded."
    },
    {
      "Id": "ChangeInDsdAttributeMandatoryStateChanged",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attribute {0} changed from {1} to {2}."
    },
    {
      "Id": "ChangeInDsdAttributeCodelistChanged",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Code list of attribute {0} changed to {1}."
    },
    {
      "Id": "ChangeInDsdAttributeAttachmentLevelChanged",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attachment level of attribute {0} changed from {1} to {2}."
    },
    {
      "Id": "ChangeInDsdAttributeAttachmentGroupChanged",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attachment group of attribute {0} changed from {1} to {2}."
    },
    {
      "Id": "ChangeInDsdAttributeRemoved",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attribute(s) removed: {0}"
    },
    {
      "Id": "ChangeInDsdDimensionCodelistCodeRemoved",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Code(s) removed from codelist {0} of dimension {1} : {2}"
    },
    {
      "Id": "ChangeInDsdAttributeCodelistCodeRemoved",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Code(s) removed from codelist {0} of attribute {1} : {2}"
    },
    {
      "Id": "ChangeInDsdDimensionCodelistCodeAdded",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Code(s) appended to codelist {0} of dimension {1} : {2}"
    },
    {
      "Id": "ChangeInDsdAttributeCodelistCodeAdded",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Code(s) appended to codelist {0} of attribute {1} : {2}"
    },
    {
      "Id": "FailedToOpenDatastoreDbConnectionForCleanup",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Failed to open datastore database [{0}] connection for cleanup of transaction [{1}]. Please contact your system administrator."
    },
    {
      "Id": "FailedToOpenManagementDbConnection",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Failed to open management database connection. Please contact your system administrator."
    },
    {
      "Id": "FailedToOpenDatastoreDbConnection",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Failed to open datastore database [{0}] connection. Please contact your system administrator."
    },
    {
      "Id": "DatabaseTableNotFound",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Table {0} not found in database. Please contact your system administrator."
    },
    {
      "Id": "AttributeNotDefinedInDsd",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attribute {3} not defined in DSD {4}."
    },
    {
      "Id": "AttributeReportedAtMultipleLevels",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attribute {3} has values reported at observation and non-observation levels. Attribute values should be reported only at one attachment level."
    },
    {
      "Id": "MandatoryDatasetAttributeWithNullValue",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attribute {3} is mandatory but empty value is provided."
    },
    {
      "Id": "MandatoryAttributeWithNullValue",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attribute {3} is mandatory but empty value is provided at key {1}."
    },
    {
      "Id": "UnknownAttributeCodeMemberWithoutCoordinate",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Unknown code for attribute {3} : {4}."
    },
    {
      "Id": "UnknownAttributeCodeMemberWithCoordinate",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Unknown code for attribute {3} at key {1} : {4}."
    },
    {
      "Id": "DuplicatedDatasetAttribute",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Values provided multiple times for attribute {3}."
    },
    {
      "Id": "DuplicatedAttributeKey",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Values provided multiple times for attribute {3} at key {1}."
    },
    {
      "Id": "WrongSetOfDimensionsInKeyOfKeyable",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Attribute {3} is reported at a keyable where the partial keys are not matching the attribute definition in DSD. Key {4} reported but combination of {5} expected."
    },
    {
      "Id": "MissingCodeMemberWithCoordinate",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Missing code for for component {3} at key {1}."
    },
    {
      "Id": "UnknownCodeMemberWithCoordinate",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Unknown code for component {3} at key {1} : {4}."
    },
    {
      "Id": "UnknownCodeMemberWithoutCoordinate",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Unknown code for component {3} : {4}."
    },
    {
      "Id": "ObservationAttributeReportedAtDataset",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Observation level attribute {3} reported at dataset level. Should be reported at observation level."
    },
    {
      "Id": "ObservationAttributeReportedAtKeyables",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Observation level attribute {3} reported at non-observation level at key {1}. Should be reported at observation level."
    },
    {
      "Id": "DatasetAttributeReportedAtKeyables",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Dataset level attribute {3} reported at dimension/group level at key {1}. Should be reported at dataset or observation level."
    },
    {
      "Id": "DimGroupAttributeReportedAtDataset",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Dimension/group level attribute {3} reported at dataset level. Should be reported at dimension/group or observation level."
    },
    {
      "Id": "MandatoryDatasetAttributeMissing",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "{3} is mandatory dataset attribute but has no value provided yet."
    },
    {
      "Id": "MandatoryDimGroupAttributeMissing",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Mandatory attribute {3} has no value provided but observation data is being loaded for coordinate: {1}"
    },
    {
      "Id": "MandatoryAttributeWithNullValueAndExistingObservationData",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Mandatory {3} attribute value cannot be deleted due to related observation data existing in database for coordinate: {1}"
    },
    {
      "Id": "MandatoryAttributeWithNullValueAndDataInStaging",
      "LanguageCode": "en",
      "Context": "Transfer",
      "Resource": "Mandatory {3} attribute value cannot be deleted due to observation data being loaded for the same coordinate: {1}"
    }
  ]
}
```
Save the File

Test the application:
- open your NSIWebservcie in a browser, and check the main page appears
- Try to run a dataflow query against the database like /rest/dataflow
