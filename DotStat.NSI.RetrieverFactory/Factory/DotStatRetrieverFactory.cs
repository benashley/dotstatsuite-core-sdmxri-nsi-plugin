﻿using System;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Security.Principal;
using DotStat.Domain;
using DotStat.NSI.DataRetriever;
using DotStat.NSI.DataRetriever.Helper;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.Ws.Retrievers.Factory;
using Estat.Sri.Ws.Retrievers.Manager;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using DotStat.Common.Logger;
using DotStat.Common.Configuration.Interfaces;
using DryIoc;
using Org.Sdmxsource.Util;

namespace DotStat.NSI.RetrieverFactory.Factory
{
    /// <summary>
    /// The DotStat implementation of retriever plugin.
    /// </summary>
    public class DotStatRetrieverFactory : SriRetrieverFactory
    {
        private readonly DryIoc.Container _container;
        private readonly IHeaderRetrievalManager _defaultHeaderRetrievalManager;
        private readonly ConnectionStringSettings _mappingStoreConnectionSettings;
        private readonly IHttpContextAccessor _contextAccessor;
        private DotStatDataRetrieverCore _dataRetriever;
        private readonly bool _allowPITTargetDataVersion;

        /// <summary>
        /// Gets the unique identifier of the implementation.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public override string Id => "DotStatRetrieverFactory";

        /// <summary>
        /// Initializes a new instance of the <see cref="SriRetrieverFactory" /> class.
        /// </summary>
        /// <param name="contextAccessor">The http context accessor.</param>
        /// <param name="defaultHeaderRetrievalManager">The default header retrieval manager.</param>
        /// <param name="mappingStoreConnectionStringBuilder">The mappingStore connection string builder.</param>
        /// <param name="connectionBuilder">The connection builder.</param>
        /// <param name="mappingManager">The mapping manager.</param>
        /// <param name="mappingValidationManager">The mapping validation manager.</param>
        /// <param name="dataflowPrincipalManager"></param>
        /// <exception cref="ArgumentNullException">
        /// defaultHeaderRetrievalManager
        /// or
        /// configManager
        /// or
        /// connectionBuilder
        /// or
        /// mappingManager
        /// or
        /// mappingValidationManager
        /// </exception>
        /// <exception cref="SdmxException">Could not establish a connection to the mapping store DB</exception>
        public DotStatRetrieverFactory(
            IHttpContextAccessor contextAccessor,
            IHeaderRetrievalManager defaultHeaderRetrievalManager,
            IConnectionStringBuilderManager mappingStoreConnectionStringBuilder,
            IBuilder<DbConnection, DdbConnectionEntity> connectionBuilder,
            IComponentMappingManager mappingManager,
            IComponentMappingValidationManager mappingValidationManager,
            IDataflowPrincipalManager dataflowPrincipalManager)
             : base(defaultHeaderRetrievalManager,
                   mappingStoreConnectionStringBuilder,
                   connectionBuilder,
                   mappingManager,
                   mappingValidationManager,
                   dataflowPrincipalManager)
        {
            Log.Configure(contextAccessor);

            Log.Debug("Constructor");

            _contextAccessor = contextAccessor;
            _container = new DotStatDataRetrieverIoc().CreateContainer();
            string dataSpace = ConfigurationManager.AppSettings["DataspaceId"];
            _allowPITTargetDataVersion = _container.Resolve<IDataspaceConfiguration>().SpacesInternal.First(space => space.Id == dataSpace).AllowPITTargetDataVersion;
            _defaultHeaderRetrievalManager = defaultHeaderRetrievalManager;
            _mappingStoreConnectionSettings = new MappingStoreSettingsManager().MappingStoreConnectionSettings;
        }

        /// <summary>
        /// Gets the data retrieval which is responsible for retrieving data for REST (any version) and SOAP v2.0.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithWriter" />.</returns>
        public override ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            var acceptHeaderOfRequest = _contextAccessor.HttpContext.Request.Headers["Accept"];

            var releaseHeaderOfRequest = _contextAccessor.HttpContext.Request.Headers["X-DotStat-Release"];
            
            var targetVersion = releaseHeaderOfRequest.Any(s => s.Equals("PIT", StringComparison.InvariantCultureIgnoreCase)) ? TargetVersion.PointInTime : TargetVersion.Live;

            if(!_allowPITTargetDataVersion && targetVersion==TargetVersion.PointInTime)
                throw new SdmxUnauthorisedException("Point in Time access disabled.");

            Log.Debug($"Data request: {acceptHeaderOfRequest}");

            return _dataRetriever = new DotStatDataRetrieverCore(
                principal,
                _container,
                _defaultHeaderRetrievalManager.Header,
                new HeaderRetrieverEngine(_mappingStoreConnectionSettings),
                base.GetDataRetrieval(sdmxSchema, principal),
                _mappingStoreConnectionSettings,
                HeaderScope.Range,
                acceptHeaderOfRequest,
                targetVersion
            );
        }

        /// <summary>
        /// Gets the advanced data retrieval which is responsible for retrieving data using the advanced SDMX v2.1 SOAP queries.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IAdvancedSdmxDataRetrievalWithWriter" />.</returns>
        public override IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal)
        {
            throw new SdmxNotImplementedException();
        }

        /// <summary>
        /// Gets the data retrieval with cross which is responsible for retrieving SDMX v2.0 Cross Sectional data for REST (any version) and SOAP v2.0.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithCrossWriter" />.</returns>
        public override ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema,
            IPrincipal principal)
        {
            throw new SdmxNotImplementedException();
        }

        public override int GetNumberOfObservations(IDataQuery dataQuery)
        {
            return _dataRetriever.GetNumberOfObservations(dataQuery);
        }
    }
}